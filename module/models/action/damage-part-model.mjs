import { CompactingMixin } from "@models/mixins/compacting-mixin.mjs";

import { FormulaField } from "@datafields/formula-field.mjs";

export class DamagePartModel extends CompactingMixin(foundry.abstract.DataModel) {
  static defineSchema() {
    const fields = foundry.data.fields;
    return {
      formula: new FormulaField(),
      types: new fields.SetField(new fields.StringField({ blank: false, nullable: false })),
    };
  }

  static migrateData(source) {
    // Since v11, merge standard and custom type IDs into single array.
    if (!source.types && source.type) {
      source.types = source.type?.values ?? [];

      if (typeof source.type?.custom === "string" && source.type.custom.length) {
        source.types.push(source.type.custom.split(";").map((t) => t.trim()));
      }
    }

    return super.migrateData(source);
  }

  _initialize(options) {
    super._initialize(options);

    Object.defineProperty(this, "type", {
      get() {
        foundry.utils.logCompatibilityWarning("DamagePartModel.type is deprecated in favor of DamagePartModel.types", {
          since: "PF1 v11",
          until: "PF1 v12",
        });

        const full = this.parsed;
        return {
          values: [...full.standard].map((d) => d.id),
          custom: [...full.custom].join(";"),
        };
      },
    });
  }

  /**
   * Prune data
   *
   * @param {object} data - Damage part data
   */
  static pruneData(data) {
    if (!data.formula) delete data.formula;
    data.types = data.types?.map((t) => t.trim()).filter((t) => !!t);
    if (!data.types?.length) delete data.types;
  }

  /**
   * Parsed types
   *
   * @type {{standard:Set<pf1.registry.DamageType>,custom:Set<string>}}
   */
  get parsed() {
    // TODO: Cache this?

    const result = {
      standard: new Set(),
      custom: new Set(),
      get all() {
        return [...this.standard, ...this.custom];
      },
    };
    for (const type of this.types) {
      const d = pf1.registry.damageTypes.get(type);
      if (d) {
        result.standard.add(d);
      } else {
        result.custom.add(type);
      }
    }
    return result;
  }

  /**
   * Parsed names of all types
   *
   * @type {Array<string>}
   */
  get names() {
    return this.parsed.all.map((dt) => dt.name || dt);
  }

  /**
   * Standard types
   *
   * @type {Set<pf1.registry.DamageType>}
   */
  get standard() {
    return this.parsed.standard;
  }

  /**
   * Custom types
   *
   * @type {Set<string>}
   */
  get custom() {
    return this.parsed.custom;
  }
}
