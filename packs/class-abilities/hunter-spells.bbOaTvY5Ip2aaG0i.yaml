_id: bbOaTvY5Ip2aaG0i
_key: '!items!bbOaTvY5Ip2aaG0i'
_stats:
  coreVersion: '12.331'
img: icons/magic/nature/beam-hand-leaves-green.webp
name: Hunter Spells
system:
  associations:
    classes:
      - Hunter
  description:
    value: >-
      <p>A hunter casts divine spells drawn from the druid and ranger spell
      lists. Only druid spells of 6th level and lower and ranger spells are
      considered to be part of the hunter spell list. If a spell appears on both
      the druid and ranger spell lists, the hunter uses the lower of the two
      spell levels listed for the spell. For instance, reduce animal is a
      2nd-level druid spell and a 3rd-level ranger spell, making it a 2nd-level
      hunter spell. Likewise, detect poison is a 0-level druid spell and a
      2nd-level ranger spell, making it a 0-level hunter spell.</p><p>The hunter
      can cast any spell she knows without preparing it ahead of time. To learn
      or cast a spell, a hunter must have a Wisdom score equal to at least 10 +
      the spell’s level. The Difficulty Class for a saving throw against a
      hunter’s spell is 10 + the spell’s level + the hunter’s Wisdom
      modifier.</p><p>A hunter cannot use spell completion or spell trigger
      magic items (without making a successful Use Magic Device check) of druid
      spells of 7th level or higher. Her alignment may restrict her from casting
      certain spells opposed to her moral or ethical beliefs; see Chaotic, Evil,
      Good, and Lawful Spells below.</p><p>Like other spellcasters, a hunter can
      cast only a certain number of spells of each spell level per day. Her base
      daily spell allotment is given on
      @UUID[Compendium.pf1.pf1e-rules.JournalEntry.GnZ4SFsgV11ab8Vz.JournalEntryPage.8HNFlPoMR5NxGMzD#class-features$3]{Table:
      Hunter}. In addition, she receives bonus spells per day if she has a high
      Wisdom score (see
      @UUID[Compendium.pf1.pf1e-rules.JournalEntry.GnZ4SFsgV11ab8Vz.JournalEntryPage.BfKeMBHQruwqyUXX]{Table:
      Ability Modifiers and Bonus Spells}).</p><p>Unlike druids and rangers, a
      hunter’s selection of spells is extremely limited. A hunter begins play
      knowing four 0-level spells and two 1st-level spells of her choice. At
      each new hunter level, she gains one or more new spells, as indicated on
      @UUID[Compendium.pf1.pf1e-rules.JournalEntry.GnZ4SFsgV11ab8Vz.JournalEntryPage.8HNFlPoMR5NxGMzD#spells-known$2]{Table:
      Spells Known}. Unlike spells per day, the number of spells a hunter knows
      is not affected by her Wisdom score; the numbers on Table: Spells Known
      are fixed.</p><p>In addition to the spells gained by hunters as they gain
      levels, each hunter also automatically adds all summon nature’s ally
      spells to her list of spells known. These spells are added as soon as the
      hunter is capable of casting them.</p><p>Unlike a druid or ranger, a
      hunter need not prepare her spells in advance. She can cast any spell she
      knows at any time, assuming she has not yet used up her spells per day for
      that spell level.</p>
  sources:
    - id: PZO1110
      pages: '49'
  subType: classFeat
type: feat
