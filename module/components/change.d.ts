export {};

declare module "./change.mjs" {
  interface ItemChange {
    _id: string;
    formula: string;
    operator?: "add" | "set";
    target: BuffTarget;
    type: string;
    priority?: number;
    value?: number;
    flavor?: string;
    continuous?: boolean;
  }
}
