import { AEBaseModel } from "./ae-base.mjs";

/**
 * Buff tracking Active Effect.
 *
 * Active Effect type: `buff`
 */
export class AEBuffModel extends AEBaseModel {
  static defineSchema() {
    const fields = foundry.data.fields;

    return {
      ...super.defineSchema(),
      // Nothing here yet
    };
  }

  /** @override */
  async _preCreate(data, context, user) {
    await super._preCreate(data, context, user);

    // Tracker does not support disabling
    if (data.disabled) this.parent.updateSource({ disabled: false });
  }

  /** @override */
  async _preUpdate(changed, context, user) {
    await super._preUpdate(changed, context, user);

    // Buff trackers do not support disabling themselves.
    delete changed.disabled;
  }

  /** @override */
  async _preDelete(context, user) {
    await super._preDelete(context, user);

    // Tracker AE can not be deleted while parent item is active
    if (this.item?.isActive) return false;
  }

  /**
   * Is this item tracking AE
   *
   * @remarks
   * - There should be only one AE with this true per item.
   *
   * @type {boolean}
   */
  get isTracker() {
    // This being false is an error of some kind.
    return !!this.item;
  }
}
