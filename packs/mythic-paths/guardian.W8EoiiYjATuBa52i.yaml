_id: W8EoiiYjATuBa52i
_key: '!items!W8EoiiYjATuBa52i'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/spells/protect-blue-3.jpg
name: Guardian
system:
  description:
    value: >-
      <p>Not all mythic characters seek glory and fame for themselves. Some take
      a more humble route, watching over those dear to them or the lands they
      call home. Guardians seek connections with those around them, even the
      beasts, and draw their power from the trust of such bonds. This doesn’t
      mean that all guardians are peaceful—indeed many use violent means to
      further their goals and safeguard those they watch over. But they find
      worth in those who travel with them. In battle, none can take the sort of
      punishment and wounds that a guardian can sustain.</p><p>Role: When others
      would retreat, you stand your ground. Your place is at the front of the
      battle, taking every hit your enemies can give while daring them to dish
      out more. Your job is to stop your enemies’ advance, interposing yourself
      in the path of peril. You’ll gladly take all that brutal punishment and
      return the beating with zeal. The wounds you suffer might kill a lesser
      hero, but you relish the thrill of combat, confident that your physical
      and mental perfection will lead to victory.</p><p>Classes: Any class that
      is frequently in the middle of a chaotic melee will find many valuable
      abilities within the guardian path. Barbarians, cavaliers, fighters,
      inquisitors, monks, and paladins all make excellent guardians. Some of the
      guardian’s powers also lend themselves to the druid, ranger, or even the
      summoner, granting bonuses to companions and allies.</p><p>Bonus Hit
      Points: Whenever you gain a guardian tier, you gain 5 bonus hit points.
      These hit points stack with themselves, and don’t affect your overall Hit
      Dice or other statistics.</p><h2>Path
      Features</h2><table><tbody><tr><td><p>Tier</p></td><td><p>Path
      Features</p></td></tr><tr><td><p>1st</p></td><td><p>Guardian's call, path
      ability</p></td></tr><tr><td><p>2nd</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>3rd</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>4th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>5th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>6th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>7th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>8th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>9th</p></td><td><p>Path
      ability</p></td></tr><tr><td><p>10th</p></td><td><p>Path ability, true
      defender</p></td></tr></tbody></table><p>As you increase in tier, you gain
      the following abilities.</p><p>Guardian’s Call: Select one of the
      following abilities. Once chosen, it can’t be changed.</p><p>Absorb Blow
      (Su): As an immediate action, whenever you take hit point damage from a
      single source (such as a dragon’s breath, a spell, or a weapon), you can
      expend one use of mythic power to reduce the damage you take from that
      source by 5 per tier (to a minimum of 0 points of damage taken). If you
      have another ability or effect that reduces damage (such as protection
      from energy), reduce the damage with the absorb blow ability before
      applying any other damage-reducing effects. For every 10 points of damage
      that this ability prevents, for 1 minute you gain DR 1/epic and 5 points
      of resistance against acid, cold, electricity, fire, and sonic damage. The
      DR and resistances stack with any other DR and resistances that you
      have.</p><p>Beast’s Fury (Su): As a swift action, you can expend one use
      of mythic power to imbue your animal companion, cohort, eidolon, familiar,
      or bonded mount with some of your mythic power. As an immediate action,
      that creature can move up to its speed and make an attack with one of its
      natural weapons. When making this attack, the creature rolls twice and
      takes the higher result. Any damage dealt by this attack bypasses all
      damage reduction. A creature affected by this ability can take these
      actions in addition to any others that it takes during its
      turn.</p><p>Sudden Block (Su): As an immediate action, you can expend one
      use of mythic power to hinder a melee attack made against you or an
      adjacent ally. Add your tier to your AC or the ally’s AC against this
      attack. The creature making the attack must make two attack rolls and take
      the lower result. Once the attack is resolved, you or your ally (your
      choice) can make one melee attack against the creature. The damage from
      this attack bypasses all damage reduction.</p><p>Path Ability: At 1st tier
      and every tier thereafter, select one new path ability from the guardian
      path abilities lists or from the universal path abilities lists (see page
      50). Once you select an ability, it can’t be changed. Unless otherwise
      noted, each ability can be selected only once. Some abilities have
      requirements, such as a class ability or minimum mythic tier, that you
      must meet before you select them.</p><p>True Defender (Su): At 10th tier,
      whenever you take damage from a melee or ranged attack scored by a
      nonmythic creature, the damage is halved. This reduction is applied after
      all other reductions in damage, such as energy resistance or damage
      reduction. Once per round, when an enemy scores a critical hit against
      you, you regain one use of mythic power.</p>
  hd: 5
  hp: 0
  sources:
    - id: PZO1126
      pages: '26'
  subType: mythic
  tag: mythicGuardian
type: class
