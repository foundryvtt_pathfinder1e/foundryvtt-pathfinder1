/**
 * Add static helper data to roll data.
 *
 * Static data:
 * - `@step`: 5 (represents single step which is always 5 internally, even if it can be 1.5m in the final result)
 *
 * @param {object} rollData - Roll data to modify
 */
export function addStatic(rollData) {
  rollData.step = 5;
}
