/**
 * Vehicle actor data model
 */
export class VehicleModel extends foundry.abstract.TypeDataModel {
  static defineSchema() {
    const fields = foundry.data.fields;

    return {
      attributes: new fields.SchemaField({
        ac: new fields.SchemaField({
          normal: new fields.SchemaField({
            bonus: new fields.NumberField({
              initial: 0,
              nullable: false,
            }),
          }),
          touch: new fields.SchemaField({
            bonus: new fields.NumberField({
              initial: 0,
              nullable: false,
            }),
          }),
          stopped: new fields.SchemaField({
            bonus: new fields.NumberField({
              initial: 0,
              nullable: false,
            }),
          }),
        }),
        hardness: new fields.SchemaField({
          base: new fields.NumberField({
            initial: 0,
            nullable: false,
            integer: true,
          }),
          bonus: new fields.NumberField({
            initial: 0,
            nullable: false,
          }),
        }),
        hp: new fields.SchemaField({
          bonus: new fields.NumberField({
            initial: 0,
            nullable: false,
            integer: true,
          }),
          offset: new fields.NumberField({
            initial: 0,
            nullable: false,
            integer: true,
          }),
        }),
        init: new fields.SchemaField({
          value: new fields.NumberField({
            initial: 0,
            nullable: false,
            integer: true,
          }),
        }),
        savingThrows: new fields.SchemaField({
          save: new fields.SchemaField({
            base: new fields.NumberField({
              initial: 0,
              nullable: false,
              integer: true,
            }),
          }),
        }),
      }),
      crew: new fields.SchemaField({
        value: new fields.NumberField({
          initial: 0,
          integer: true,
          min: 0,
          nullable: false,
        }),
        max: new fields.NumberField({
          initial: 1,
          integer: true,
          min: 1,
          nullable: false,
        }),
        passengers: new fields.NumberField({
          initial: 0,
          integer: true,
          min: 0,
          nullable: false,
        }),
      }),
      currency: new fields.SchemaField({
        pp: new fields.NumberField({
          initial: 0,
          min: 0,
          nullable: false,
          integer: true,
        }),
        gp: new fields.NumberField({
          initial: 0,
          min: 0,
          nullable: false,
          integer: true,
        }),
        sp: new fields.NumberField({
          initial: 0,
          min: 0,
          nullable: false,
          integer: true,
        }),
        cp: new fields.NumberField({
          initial: 0,
          min: 0,
          nullable: false,
          integer: true,
        }),
      }),
      material: new fields.SchemaField({
        base: new fields.StringField({}),
        magicallyHardened: new fields.BooleanField({
          initial: false,
        }),
      }),
      squares: new fields.NumberField({
        initial: 2,
        integer: true,
        min: 2,
        nullable: false,
      }),
      traits: new fields.SchemaField({
        size: new fields.StringField({
          required: true,
          initial: "lg",
          choices: () => pf1.config.actorSizes,
        }),
        type: new fields.StringField({
          required: true,
          initial: "land",
          choices: () => pf1.config.vehicles.type,
        }),
      }),
      details: new fields.SchemaField({
        carryCapacity: new fields.SchemaField({}),
        acceleration: new fields.NumberField({
          initial: 0,
          min: 0,
          integer: true,
          nullable: false,
        }),
        cost: new fields.NumberField({
          initial: 0,
          min: 0,
          nullable: false,
        }),
        currentSpeed: new fields.NumberField({
          initial: 0,
          min: 0,
          integer: true,
          nullable: false,
        }),
        decks: new fields.NumberField({
          initial: 0,
          min: 0,
          integer: true,
          nullable: false,
        }),
        description: new fields.HTMLField({}),
        drivingCheck: new fields.StringField({}),
        drivingDevice: new fields.StringField({}),
        drivingSpace: new fields.StringField({}),
        forwardFacing: new fields.StringField({}),
        maxSpeed: new fields.NumberField({
          initial: 0,
          min: 0,
          integer: true,
          nullable: false,
        }),
        propulsion: new fields.StringField({}),
        notes: new fields.HTMLField(),
      }),
      driver: new fields.SchemaField({
        skill: new fields.StringField(),
        uuid: new fields.StringField(),
      }),
    };
  }

  static migrateData(source) {
    if (!source.details) return;

    // Migrate Hardess
    if (typeof source.details.hardness === "number") {
      source.attributes.hardness ??= {
        base: source.details.hardness,
        bonus: 0,
      };

      delete source.details.hardness;
    }

    // Migrate Crew
    if (Number.isInteger(source.details.crew)) {
      source.details.crew = {
        value: source.details.crew,
        max: source.details.crew,
        passengers: 0,
      };
    }

    // Migrate Notes
    if (typeof source.details?.notes?.value === "string") {
      source.details.notes = source.details.notes.value;
    }
  }
}
